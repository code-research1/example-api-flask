from flask import Flask, make_response
import logging
from datetime import datetime

from src.constants import infoConstant as constant
from src.api.middlewares.corsMiddleware import CorsMiddleware
from src.api.routes.mainRoute import router
from src.utils.commonUtils import CommonUtils as commonUtils
from src.utils.envUtils import EnvUtils as envUtils
from src.utils.databaseUtils import db
from src.utils.databaseUtils import DatabaseUtils as databaseUtils
from src.utils.loggerUtils import CustomJSONFormatter

def create_app():

    app = Flask(__name__)

    # Allow cross-domain requests
    CorsMiddleware(app)

    # Configure the environment
    envUtils.LoadEnvConfig(app)
    config = envUtils.NewEnvConfig()

    # Register the main router
    app.register_blueprint(router, url_prefix = config['route'])

    # Configure the database connection to a specific schema
    app.config['SQLALCHEMY_DATABASE_URI'] = databaseUtils.DatabaseConfig()
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    # Define the log file path
    currentDate = datetime.now().strftime('%d-%b-%Y')
    logFile = f"{config['logDirectory']}{config['appName']} {currentDate}.log"

    # Set the log level (e.g., INFO, DEBUG, ERROR)
    logLevel = logging.INFO

    # Create a FileHandler and set the log level
    fileHandler = logging.FileHandler(logFile)
    fileHandler.setLevel(logLevel)

    # Set the custom formatter for the FileHandler
    fileHandler.setFormatter(CustomJSONFormatter())

    # Add the FileHandler to the Flask app logger
    app.logger.addHandler(fileHandler)


    # Handle default 404 exceptions with a custom response
    @app.errorhandler(404)
    def resource_not_found(exception):
        response = commonUtils.GeneralResponse(404, constant.NOT_FOUND, constant.ERR_MESSAGE_NOT_FOUND)
        return make_response(response, 404)

    # Handle default 500 exceptions with a custom response
    @app.errorhandler(500)
    def internal_server_error(error):
        response = commonUtils.GeneralResponse(500, constant.INTERNAL_SERVER_ERROR, constant.ERR_MESSAGE_INTERNAL)
        return make_response(response, 500)

    return app

app = create_app()

if __name__ == '__main__':
    app.run(debug=app.config['releaseMode'] == 'n', port = app.config['serverPort'])
