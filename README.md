# Flask(python) Clean Architecture

This project was created to learn python with Flask framework

## Installation

1. path cd .... folder app
2. Run with cmd administrator 
    - [x] py -3 -m venv .venv
    - [x]  .venv\Scripts\activate
    - [x]  pip install Flask
    - [x]  pip list
    - [x]  pip install -r requirements.txt 
    - [x]  (pip freeze > requirements.txt optional)

## How To Run

1. Run application with command `python app.py`

## Feature

- [x] Database ORM
- [x] Database Relational
- [x] Json Validation
- [x] JWT Security
- [x] Open API / Swagger //===TODO
- [x] Http Client
- [x] Error Handling
- [x] Logging
- [x] Repository Pattern
- [x] Encrypt & Decrypt
- [x] Xtra (Exmple Database & Json)

