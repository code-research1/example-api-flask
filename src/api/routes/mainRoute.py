from flask import Blueprint
import sys

from src.utils.envUtils import EnvUtils as envUtils
from src.api.routes.authRoute import authRoute
from src.api.routes.userRoute import userRoute
from src.api.routes.roleRoute import roleRoute
from src.api.routes.commonRoute import commonRoute
from src.utils.loggerUtils import CustomJSONFormatter as logger

router = Blueprint('main', __name__)

@router.route('/')
def index():
    appName = envUtils.get('appName')

    if appName is not None:
        return f"Welcome to Web Service {appName}"
    else:
        logger.CreateLog("ERROR", "Welcome to Web Service (App Name Not Found)", 500, sys._getframe().f_lineno, None)
        return "Welcome to Web Service (App Name Not Found)"

router.register_blueprint(authRoute)
router.register_blueprint(userRoute)
router.register_blueprint(roleRoute)
router.register_blueprint(commonRoute)
