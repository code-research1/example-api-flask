from flask import Blueprint

from src.api.controllers.commonController import CommonController


commonRoute = Blueprint('commonRoute', __name__)

@commonRoute.route('/common/encrypt', methods=['POST'])
def Encrypt():
    getCommonController = CommonController()
    response = getCommonController.encrypt()
    return response

@commonRoute.route('/common/decrypt', methods=['POST'])
def Decrypt():
    getCommonController = CommonController()
    response = getCommonController.decrypt()
    return response