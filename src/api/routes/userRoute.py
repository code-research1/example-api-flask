from flask import Blueprint

from src.api.controllers.userController import UserController
from src.api.middlewares.authMiddleware import AuthMiddleware as authMiddleware

userRoute = Blueprint('userRoute', __name__)

@userRoute.route('/user', methods=['GET'])
@authMiddleware.ValidateToken
def FindAll(data):
    getUserController = UserController()
    response = getUserController.getAllDataUser()
    return response

@userRoute.route('/user/getDataById/<string:idMasterUsers>', methods=['GET'])
@authMiddleware.ValidateToken
def FindById(data, idMasterUsers):
    getUserController = UserController()
    response = getUserController.getDataUserById(idMasterUsers)
    return response


@userRoute.route('/user/getAllDataUserByParam', methods=['POST'])
@authMiddleware.ValidateToken
def GetAllDataUserByParam(data):
    getUserController = UserController()
    response = getUserController.getAllDataUserByParam()
    return response

@userRoute.route('/user/store', methods=['POST'])
@authMiddleware.ValidateToken
def Store(data):
    getUserController = UserController()
    response = getUserController.store()
    return response

@userRoute.route('/user/update', methods=['POST'])
@authMiddleware.ValidateToken
def Update(data):
    getUserController = UserController()
    response = getUserController.update()
    return response

@userRoute.route('/user/updateIsActive', methods=['POST'])
@authMiddleware.ValidateToken
def UpdateIsActive(data):
    getUserController = UserController()
    response = getUserController.updateIsActive()
    return response