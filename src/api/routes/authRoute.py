from flask import Blueprint, jsonify
from src.api.controllers.authController import AuthController

authRoute = Blueprint('authRoute', __name__)

@authRoute.route('/login', methods=['POST'])
def FindAll():
    getAuthController = AuthController()
    response = getAuthController.signIn()
    return response
