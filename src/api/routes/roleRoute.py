from flask import Blueprint

from src.api.controllers.roleController import RoleController
from src.api.middlewares.authMiddleware import AuthMiddleware as authMiddleware


roleRoute = Blueprint('roleRoute', __name__)

@roleRoute.route('/role', methods=['GET'])
@authMiddleware.ValidateToken
def FindAll(data):
    getRoleController = RoleController()
    response = getRoleController.getAllDataRole()
    return response


@roleRoute.route('/role/getDataById/<string:idMasterRoles>', methods=['GET'])
@authMiddleware.ValidateToken
def FindById(data, idMasterRoles):
    getRoleController = RoleController()
    response = getRoleController.getDataRoleById(idMasterRoles)
    return response

@roleRoute.route('/role/store', methods=['POST'])
@authMiddleware.ValidateToken
def Store(data):
    getRoleController = RoleController()
    response = getRoleController.store()
    return response

@roleRoute.route('/role/update', methods=['POST'])
@authMiddleware.ValidateToken
def Update(data):
    getRoleController = RoleController()
    response = getRoleController.update()
    return response

@roleRoute.route('/role/updateIsActive', methods=['POST'])
@authMiddleware.ValidateToken
def UpdateIsActive(data):
    getRoleController = RoleController()
    response = getRoleController.updateIsActive()
    return response