from src.utils.databaseUtils import db
from src.api.models.entity.userEntity import UserEntity
from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import applicationConstant as constant

class AuthRepository:

    @staticmethod
    def checkLogin(username: str, password: str):
        results = UserEntity.query.filter_by(username=username).first()
        
        if results is None:
            return results
        else:
            decryptPassword = commonUtils.DecryptAes256Sha256(password, constant.PASSWORD)
            if commonUtils.VerifyPassword(results.password, decryptPassword):
                return results
            else:
                results = None
                return results

        
     