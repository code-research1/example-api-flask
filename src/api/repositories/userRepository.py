from src.utils.databaseUtils import db
from src.api.models.entity.userEntity import UserEntity

class UserRepository:
    @staticmethod
    def getAllDataUsers():
        return UserEntity.query.order_by(UserEntity.fullname).all()

    @staticmethod
    def getDataUserById(userId: int):
        results = UserEntity.query.filter_by(idMasterUsers=userId).first()
        return results
    
    @staticmethod
    def getAllDataUserByParam(key: str, value: str):
        results = UserEntity.query.filter(UserEntity.email == value).all()
        return results

    @staticmethod
    def store(paramStore):
        try:
            db.session.add(paramStore)
            db.session.commit()
            return paramStore
        except Exception as e:
            db.session.rollback()
            raise e 

    @staticmethod
    def update(paramStore):
        try:
            db.session.commit()
            return paramStore
        except Exception as e:
            db.session.rollback()
            raise e       
