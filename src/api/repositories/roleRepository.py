from src.utils.databaseUtils import db
from src.api.models.entity.roleEntity import RoleEntity

class RoleRepository:
    @staticmethod
    def getAllDataRoles():
        return  RoleEntity.query.order_by(RoleEntity.roleName).all()

    @staticmethod
    def getDataRoleById(roleId: int):
        results = RoleEntity.query.filter_by(idMasterRoles=roleId).first()
        return results

    @staticmethod
    def store(paramStore):
        try:
            db.session.add(paramStore)
            db.session.commit()
            return paramStore
        except Exception as e:
            db.session.rollback()
            raise e 

    @staticmethod
    def update(paramStore):
        try:
            db.session.commit()
            return paramStore
        except Exception as e:
            db.session.rollback()
            raise e       
