from flask_smorest import Blueprint
from flask.views import MethodView
import sys

from src.api.models.criteria.authCriteria import AuthCriteria
from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import infoConstant as constant
from src.api.repositories.authRepository import AuthRepository as authRepository
from src.api.middlewares.authMiddleware import AuthMiddleware as authMiddleware
from src.utils.loggerUtils import CustomJSONFormatter as logger

authBlueprint = Blueprint('auth', __name__, description='Auth operations')

class AuthController(MethodView):

    @authBlueprint.arguments(AuthCriteria, location='json')
    def signIn(self, args):
        try:
            requestModel = args
            existingUser = authRepository.checkLogin(requestModel['username'], requestModel['password'])
            if existingUser is None:
                logger.CreateLog(constant.ERROR, constant.ERR_PASSWORD_NOT_MATCH, 404, sys._getframe().f_lineno, requestModel)
                response = commonUtils.GeneralResponse(404, constant.GENERAL_ERROR, constant.ERR_PASSWORD_NOT_MATCH)
                return response

            accessToken = authMiddleware.GenerateToken()
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, accessToken) 
            return response
        except Exception as e:
            logger.CreateLog(constant.ERROR, constant.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            return commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))