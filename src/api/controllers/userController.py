from flask_smorest import Blueprint
from flask.views import MethodView
import uuid
import sys

from src.api.models.entity.userEntity import UserEntity
from src.api.models.criteria.userCriteria import UserSearchCriteria, StoreUserCriteria, UpdateUserCriteria, UpdateIsActiveUserCriteria
from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import infoConstant as constant
from src.api.repositories.userRepository import UserRepository as userRepository
from src.utils.loggerUtils import CustomJSONFormatter as logger  

blp = Blueprint('user', __name__, description='User operations')

class UserController(MethodView):

    def getAllDataUser(self):
        datas = userRepository.getAllDataUsers()
        # this is one line for datas
        listDataUsers = [data.serialize() for data in datas]
        response = commonUtils.GeneralResponse(200, constant.SUCCESS, listDataUsers)
        return response
    
    def getDataUserById(self, idMasterUsers):
        existingUser = userRepository.getDataUserById(idMasterUsers)
        if existingUser is None:
            logger.CreateLog(constant.SUCCESS, constant.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, existingUser)
            response = commonUtils.GeneralResponse(404, constant.SUCCESS, constant.DATA_NOT_FOUND)
            return response
        
        listDataUsers = existingUser.serialize()
        response = commonUtils.GeneralResponse(200, constant.SUCCESS, listDataUsers)
        return response
    
    @blp.arguments(UserSearchCriteria, location='json')
    def getAllDataUserByParam(self, args):

        try:
            requestModel = args
            result = userRepository.getAllDataUserByParam(requestModel['key'], requestModel['value'])
            listDataUsers = [data.serialize() for data in result]
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, listDataUsers)
        except Exception as e:
            logger.CreateLog(constant.ERROR, constant.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))

        return response
    
    @blp.arguments(StoreUserCriteria, location='json')
    def store(self, args):

        try:
            requestModel = args
            paramStore = UserEntity(
                idMasterUsers   = str(uuid.uuid4()),
                idMasterRoles   = requestModel['idMasterRoles'],
                fullname        = requestModel['fullname'],
                username        = requestModel['username'],
                isGender        = requestModel['isGender'],
                address         = requestModel['address'],
                hpNumber        = requestModel['hpNumber'],
                email           = requestModel['email'],
                password        = commonUtils.GeneratePassword(),
                createdBy       = requestModel['createdBy'],
                isActive        = constant.ACTIVED,
            )
            
            userRepository.store(paramStore)
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, constant.SUCCESSFULLY_ADD)
        except Exception as e:
            logger.CreateLog(constant.ERROR, constant.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))

        return response

    
    @blp.arguments(UpdateUserCriteria, location='json')
    def update(self, args):
        try:
            requestModel  = args
            existingUser  = userRepository.getDataUserById(requestModel['idMasterUsers'])
            if existingUser is None:
                logger.CreateLog(constant.SUCCESS, constant.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, existingUser)
                response = commonUtils.GeneralResponse(404, constant.SUCCESS, constant.DATA_NOT_FOUND)
                return response
            
            existingUser.idMasterRoles  = requestModel['idMasterRoles']
            existingUser.fullname       = requestModel['fullname']
            existingUser.username       = requestModel['username']
            existingUser.isGender       = requestModel['isGender']
            existingUser.address        = requestModel['address']
            existingUser.hpNumber       = requestModel['hpNumber']
            existingUser.email          = requestModel['email']
            existingUser.updatedBy      = requestModel['updatedBy']

            userRepository.update(existingUser)
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, constant.SUCCESSFULLY_UPDATE)
            return response
        except Exception as e:
            logger.CreateLog(constant.ERROR, constant.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            return commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))
    
    @blp.arguments(UpdateIsActiveUserCriteria, location='json')
    def updateIsActive(self, args):
        try:
            requestModel  = args
            existingUser  = userRepository.getDataUserById(requestModel['idMasterUsers'])
            if existingUser is None:
                logger.CreateLog(constant.SUCCESS, constant.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, existingUser)
                response = commonUtils.GeneralResponse(404, constant.SUCCESS, constant.DATA_NOT_FOUND)
                return response
            
            existingUser.isActive = requestModel['isActive']
            existingUser.updatedBy = requestModel['updatedBy']

            userRepository.update(existingUser)
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, constant.SUCCESSFULLY_DELETE)
            return response
        except Exception as e:
            logger.CreateLog(constant.ERROR, constant.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            return commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))
