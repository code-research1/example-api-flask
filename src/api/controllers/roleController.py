from flask_smorest import Blueprint
from flask.views import MethodView
import uuid
import sys

from src.api.models.entity.roleEntity import RoleEntity
from src.api.models.criteria.roleCriteria import StoreRoleCriteria, UpdateRoleCriteria, UpdateIsActiveRoleCriteria
from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import infoConstant as constant
from src.api.repositories.roleRepository import RoleRepository as roleRepository
from src.utils.loggerUtils import CustomJSONFormatter as logger  

blp = Blueprint('role', __name__, description='Role operations')

class RoleController(MethodView):

    def getAllDataRole(self):
        datas = roleRepository.getAllDataRoles()
        listDataRoles = []
        for data in datas:
            role = {
                'idMasterRoles': data.idMasterRoles,
                'roleName': data.roleName,
                'description': data.description,
                'createdBy': data.createdBy,
                'updatedBy': data.updatedBy,
                'isActive': data.isActive,
                'createdAt': data.createdAt.isoformat(),
                'updatedAt': data.updatedAt.isoformat() if data.updatedAt else None
            }
            listDataRoles.append(role)
        response = commonUtils.GeneralResponse(200, constant.SUCCESS, listDataRoles)
        return response
    
    def getDataRoleById(self, idMasterRoles):
        existingRole = roleRepository.getDataRoleById(idMasterRoles)
        if existingRole is None:
            logger.CreateLog(constant.SUCCESS, constant.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, existingRole)
            response = commonUtils.GeneralResponse(404, constant.SUCCESS, constant.DATA_NOT_FOUND)
            return response
        
        listDataRoles = existingRole.serialize()
        response = commonUtils.GeneralResponse(200, constant.SUCCESS, listDataRoles)
        return response
    
    @blp.arguments(StoreRoleCriteria, location='json')
    def store(self, args):
        try:
            requestModel = args
            paramStore = RoleEntity(
                idMasterRoles   = str(uuid.uuid4()),
                roleName        = requestModel['roleName'],
                description     = requestModel['description'],
                createdBy       = requestModel['createdBy'],
                isActive        = constant.ACTIVED,
            )
            roleRepository.store(paramStore)
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, constant.SUCCESSFULLY_ADD)
        except Exception as e:
            logger.CreateLog(constant.SUCCESS, constant.DATA_NOT_FOUND, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))

        return response

    
    @blp.arguments(UpdateRoleCriteria, location='json')
    def update(self, args):
        try:
            requestModel  = args
            existingRole  = roleRepository.getDataRoleById(requestModel['idMasterRoles'])
            if existingRole is None:
                logger.CreateLog(constant.SUCCESS, constant.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, existingRole)
                response = commonUtils.GeneralResponse(404, constant.SUCCESS, constant.DATA_NOT_FOUND)
                return response
            
            existingRole.roleName    = requestModel['roleName']
            existingRole.description = requestModel['description']
            existingRole.updatedBy   = requestModel['updatedBy']

            roleRepository.update(existingRole)
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, constant.SUCCESSFULLY_UPDATE)
            return response
        except Exception as e:
            logger.CreateLog(constant.ERROR, constant.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            return commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))
    
    @blp.arguments(UpdateIsActiveRoleCriteria, location='json')
    def updateIsActive(self, args):
        try:
            requestModel  = args
            existingRole  = roleRepository.getDataRoleById(requestModel['idMasterRoles'])
            if existingRole is None:
                logger.CreateLog(constant.SUCCESS, constant.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, existingRole)
                response = commonUtils.GeneralResponse(404, constant.SUCCESS, constant.DATA_NOT_FOUND)
                return response
            
            existingRole.isActive  = requestModel['isActive']
            existingRole.updatedBy = requestModel['updatedBy']

            roleRepository.update(existingRole)
            response = commonUtils.GeneralResponse(200, constant.SUCCESS, constant.SUCCESSFULLY_DELETE)
            return response
        except Exception as e:
            logger.CreateLog(constant.ERROR, constant.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            return commonUtils.GeneralResponse(500, constant.GENERAL_ERROR, str(e))
