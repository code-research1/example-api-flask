from flask import request, jsonify
from flask_smorest import Blueprint
from flask.views import MethodView
import os
import base64 

from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import applicationConstant as constant

blp = Blueprint('common', __name__, description='Common operations')

class CommonController(MethodView):

    def encrypt(self):
        plaintext = request.json.get('data')
        keyType = request.json.get('keyType')
        if keyType == constant.DATA:
            password = constant.KEY_AES.encode('utf-8')
        else:
            password = constant.KEY_PASS_AES.encode('utf-8')
        salt = os.urandom(16)

        key = commonUtils.GenerateKey(password, salt)
        encryptedData = commonUtils.EncryptData(key, plaintext.encode('utf-8'))

        # Include the salt in the ciphertext (prepend it to the ciphertext)
        ciphertext = salt + encryptedData

        # Encode the ciphertext and return it
        ciphertextBase64 = base64.b64encode(ciphertext).decode('utf-8')

        return jsonify({'ciphertext': ciphertextBase64})

    def decrypt(self):
        encryptedData = request.json.get('encryptedData')
        keyType = request.json.get('keyType')
        if keyType == constant.DATA:
            password = constant.KEY_AES.encode('utf-8')
        else:
            password = constant.KEY_PASS_AES.encode('utf-8')

        # Decode the Base64-encoded data
        data = base64.b64decode(encryptedData.encode('utf-8'))

        # Extract the salt from the ciphertext
        salt = data[:16]
        ciphertext = data[16:]

        key = commonUtils.GenerateKey(password, salt)
        decryptedData = commonUtils.DecryptData(key, ciphertext)

        return jsonify({'plaintext': decryptedData.decode('utf-8')})
    
