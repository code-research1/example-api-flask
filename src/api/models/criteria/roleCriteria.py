from marshmallow import Schema, fields

class StoreRoleCriteria(Schema):
    roleName = fields.Str(required=True)
    description = fields.Str(required=True)
    createdBy = fields.Str(required=True)

class UpdateRoleCriteria(Schema):
    idMasterRoles = fields.Str(required=True)
    roleName = fields.Str(required=True)
    description = fields.Str(required=True)
    updatedBy = fields.Str(required=True)    

class UpdateIsActiveRoleCriteria(Schema):
    idMasterRoles = fields.Str(required=True)
    isActive = fields.Str(required=True)
    updatedBy = fields.Str(required=True)   
