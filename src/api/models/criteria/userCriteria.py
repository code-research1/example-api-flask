from marshmallow import Schema, fields

class UserSearchCriteria(Schema):
    key = fields.Str(required=True)
    value = fields.Str(required=True)

class StoreUserCriteria(Schema):
    idMasterRoles = fields.Str(required=True)
    fullname = fields.Str(required=True)
    username = fields.Str(required=True)
    isGender = fields.Str(required=True)   
    address = fields.Str(required=True)   
    hpNumber = fields.Str(required=True)   
    email = fields.Str(required=True)   
    createdBy = fields.Str(required=True)   

class UpdateUserCriteria(Schema):
    idMasterUsers = fields.Str(required=True)
    idMasterRoles = fields.Str(required=True)
    fullname = fields.Str(required=True)
    username = fields.Str(required=True)
    isGender = fields.Str(required=True)   
    address = fields.Str(required=True)   
    hpNumber = fields.Str(required=True)   
    email = fields.Str(required=True)   
    updatedBy = fields.Str(required=True)     

class UpdateIsActiveUserCriteria(Schema):
    idMasterUsers = fields.Str(required=True)
    isActive = fields.Str(required=True)
    updatedBy = fields.Str(required=True)   
