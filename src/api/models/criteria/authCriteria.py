from marshmallow import Schema, fields

class AuthCriteria(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)