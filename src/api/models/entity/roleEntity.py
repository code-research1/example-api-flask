from datetime import datetime

from src.utils.databaseUtils import db


class TimestampMixin:
    createdAt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, name='created_at')
    updatedAt = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, name='updated_at')

class RoleEntity(TimestampMixin, db.Model):
    __tablename__ = 'master_roles'
    __table_args__ = {'schema': 'db_staterkit'} 

    idMasterRoles = db.Column(db.String(), unique=True, primary_key=True, name='id_master_roles')
    roleName = db.Column(db.String(), nullable=False, name='role_name')
    description = db.Column(db.String(), nullable=True)
    createdBy = db.Column(db.String(), nullable=False, name='created_by')
    updatedBy = db.Column(db.String(), nullable=True, name='updated_by')
    isActive = db.Column(db.String(), nullable=False, name='is_active')

    def __init__(self, idMasterRoles, roleName, description, createdBy, isActive, updatedBy=None):
        self.idMasterRoles = idMasterRoles
        self.roleName = roleName
        self.description = description
        self.createdBy = createdBy
        self.isActive = isActive
        self.updatedBy = updatedBy    
    
    def serialize(self):
        return {
            'idMasterRoles': self.idMasterRoles,
            'roleName': self.roleName,
            'description': self.description,
            'createdBy': self.createdBy,
            'updatedBy': self.updatedBy,
            'isActive': self.isActive,
            'createdAt': self.createdAt.isoformat(),
            'updatedAt': self.updatedAt.isoformat() if self.updatedAt else None
        }

