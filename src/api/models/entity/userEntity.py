from datetime import datetime

from src.utils.databaseUtils import db

class TimestampMixin:
    createdAt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, name='created_at')
    updatedAt = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, name='updated_at')

class UserEntity(TimestampMixin, db.Model):
    __tablename__ = 'master_users'
    __table_args__ = {'schema': 'db_staterkit'} 

    idMasterUsers = db.Column(db.String(), unique=True, primary_key=True, name='id_master_users')
    idMasterRoles = db.Column(db.String(), nullable=False, name= 'id_master_roles')
    fullname = db.Column(db.String(), nullable=False, name='fullname')
    username = db.Column(db.String(), nullable=False, name='username')
    isGender = db.Column(db.String(), nullable=False, name='is_gender')
    address = db.Column(db.String(), nullable=True, name='address')
    hpNumber = db.Column(db.String(), nullable=False, name='hp_number')
    dateActivation = db.Column(db.DateTime, nullable=True, name='date_activation')
    email = db.Column(db.String(), nullable=False, name='email')
    emailVerifiedAt = db.Column(db.DateTime, nullable=True, name='email_verified_at')
    password = db.Column(db.String(), nullable=False, name='password')
    urlPhoto = db.Column(db.String(), nullable=True, name='url_photo')
    createdBy = db.Column(db.String(), nullable=False, name='created_by')
    updatedBy = db.Column(db.String(), nullable=True, name='updated_by')
    isActive = db.Column(db.String(), nullable=False, name='is_active')
    
    def __init__(self, idMasterUsers, idMasterRoles, fullname, username, isGender
                 , address, hpNumber, email, password, createdBy
                 , isActive
                 , updatedBy=None, dateActivation=None, emailVerifiedAt=None, urlPhoto=None):
        self.idMasterUsers = idMasterUsers
        self.idMasterRoles = idMasterRoles
        self.fullname = fullname
        self.username = username
        self.isGender = isGender
        self.address = address
        self.hpNumber = hpNumber
        self.dateActivation = dateActivation
        self.email = email
        self.emailVerifiedAt = emailVerifiedAt
        self.password = password
        self.urlPhoto = urlPhoto
        self.createdBy = createdBy
        self.isActive = isActive
        self.updatedBy = updatedBy    
    
    def serialize(self):
        return {
            'idMasterUsers': self.idMasterUsers,
            'idMasterRoles': self.idMasterRoles,
            'fullname': self.fullname,
            'username': self.username,
            'isGender': self.isGender,
            'address': self.address,
            'hpNumber': self.hpNumber,
            'dateActivation': self.dateActivation,
            'email': self.email,
            'emailVerifiedAt': self.emailVerifiedAt,
            'createdBy': self.createdBy,
            'updatedBy': self.updatedBy,
            'isActive': self.isActive,
            'createdAt': self.createdAt.isoformat(),
            'updatedAt': self.updatedAt.isoformat() if self.updatedAt else None
        }

