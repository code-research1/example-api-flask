from datetime import datetime, timedelta
import uuid
import jwt
import hashlib
import hmac
import base64
from functools import wraps
from flask import request, jsonify
import sys


from src.utils.envUtils import EnvUtils as envUtils
from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import infoConstant as constant
from src.utils.loggerUtils import CustomJSONFormatter as logger  

class AuthMiddleware:

    @staticmethod
    def GenerateToken():
        config = envUtils.NewEnvConfig()

        payload = {
            "authorized": True,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(hours=3),
            "iss": config['apiKeyEncode'],
            "jti": str(uuid.uuid4())
        }

        token = jwt.encode(payload, config['apiKey'], algorithm='HS256') 
        return token

    @staticmethod
    def ValidateToken(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            try:
                config = envUtils.NewEnvConfig()
                token = None
                authHeader = request.headers.get('Authorization')
                apiKeyHeader = request.headers.get('Api-Key')
                signatureHeader = request.headers.get('Signature')
                signatureTimeHeader = request.headers.get('Signature-Time')

                apiKeyEncode = config['apiKeyEncode'].encode()
                signatureEncode = config['signatureKeyEncode'].encode()

                if apiKeyHeader != apiKeyEncode and apiKeyHeader is None:
                    logger.CreateLog(constant.ERROR, constant.ERR_MESSAGE_HEADER_API, 401, sys._getframe().f_lineno, apiKeyHeader)
                    response = commonUtils.GeneralResponse(401, constant.ERROR, constant.ERR_MESSAGE_HEADER_API)
                    return response
                    
                if signatureHeader != signatureEncode and signatureHeader is None:
                    logger.CreateLog(constant.ERROR, constant.ERR_MESSAGE_HEADER_SIGNATURE, 401, sys._getframe().f_lineno, signatureHeader)
                    response = commonUtils.GeneralResponse(401, constant.ERROR, constant.ERR_MESSAGE_HEADER_SIGNATURE)
                    return response
                    
                if signatureTimeHeader is None:
                    logger.CreateLog(constant.ERROR, constant.ERR_MESSAGE_HEADER_SIGNATURE_TIME, 401, sys._getframe().f_lineno, signatureTimeHeader)
                    response = commonUtils.GeneralResponse(401, constant.ERROR, constant.ERR_MESSAGE_HEADER_SIGNATURE_TIME)
                    return response

                parts = authHeader.split()

                if len(parts) == 2 and parts[0].lower() == 'bearer':
                    token = parts[1]
                else:
                    response = commonUtils.GeneralResponse(401, constant.ERROR, constant.TOKEN_INVALID)
                    return response

                if not token:
                    response = commonUtils.GeneralResponse(401, constant.ERROR, constant.TOKEN_PROVIDED)
                    return response

                try:
                    data = jwt.decode(token, config['apiKey'], algorithms=['HS256'])
                    return f(data, *args, **kwargs)
                except jwt.ExpiredSignatureError:
                    logger.CreateLog(constant.ERROR, constant.PROVIDED_TOKEN_EXPIRED, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constant.ERROR, constant.PROVIDED_TOKEN_EXPIRED)
                    return response
                except jwt.InvalidTokenError:
                    logger.CreateLog(constant.ERROR, constant.TOKEN_INVALID, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constant.ERROR, constant.TOKEN_INVALID)
                    return response

            except Exception as e:
                print("Error:", str(e))
                print("authHeader:", authHeader)
                print("token:", token)
                logger.CreateLog(constant.ERROR, constant.INTERNAL_SERVER_ERROR, 500, sys._getframe().f_lineno, None)
                response = commonUtils.GeneralResponse(500, constant.ERROR, {'message': constant.INTERNAL_SERVER_ERROR})
                return response

        return decorated
