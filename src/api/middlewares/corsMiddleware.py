from flask_cors import CORS

class CorsMiddleware:
    def __init__(self, app):
        self.app = app
        self.cors = CORS(app, resources={
            r"/api/*": {
                "origins": "https://example.com",
                "methods": ["GET", "POST", "PUT", "HEAD", "OPTIONS"],
                "allow_headers": ["*"],
                "expose_headers": ["Content-Type"],
                "supports_credentials": True,
            }
        })