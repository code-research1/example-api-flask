from flask import Blueprint, current_app, request, url_for
import logging
from datetime import datetime
import json
import sys

class CustomJSONFormatter(logging.Formatter):
    def format(self, record):
        logData = {
            "log.level": record.levelname,
            "@timestamp": datetime.now().strftime('%Y-%m-%dT%H:%M:%S%z'),
            "message": record.getMessage(),
            "title": record.title,
            "code": record.code,
            "endpoint": record.endpoint,
            "linenumber": record.linenumber,
            "datas": record.datas  
        }
        return json.dumps(logData)
    
    def CreateLog(title: str, message: str, code: int, linenumber: str, datas: any):
        return current_app.logger.error(message, extra={
            'title': title,
            'code': code,
            'endpoint': url_for(request.endpoint),  
            'linenumber': linenumber,
            'datas': datas
        })
