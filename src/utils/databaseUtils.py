from flask_sqlalchemy import SQLAlchemy


from src.utils.envUtils import EnvUtils as envUtils
from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import applicationConstant as constant

db = SQLAlchemy()

class DatabaseUtils():

    @staticmethod
    def DatabaseConfig():
        
        config = envUtils.NewEnvConfig()
        environment = config['environment']

        if config is not None:
            connection = commonUtils.DecryptAes256Sha256(config['connection'], constant.DATA)
            username = commonUtils.DecryptAes256Sha256(config['username'], constant.DATA)
            password = commonUtils.DecryptAes256Sha256(config['password'], constant.DATA)
            url = commonUtils.DecryptAes256Sha256(config['url'], constant.DATA)
            port = commonUtils.DecryptAes256Sha256(config['port'], constant.DATA)
            dialect = commonUtils.DecryptAes256Sha256(config['dialect'], constant.DATA)
            
            if connection is not None:
                # Construct the connection string
                connectionString = f'{connection}://{username}:{password}@{url}:{port}/{dialect}'
                return connectionString
            else:
                return print("Database connection not found in the configuration.")
        else:
            return print(f"Configuration for environment '{environment}' not found in the configuration.")
