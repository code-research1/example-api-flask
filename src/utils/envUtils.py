
import yaml

class EnvUtils:
    _config = None 
    
    @staticmethod
    def LoadEnvConfig(app):
        with open('config.yaml', 'r') as stream:
            config = yaml.safe_load(stream)
        app.config.update(config)
        # Set the _config attribute to the loaded configuration
        EnvUtils._config = config
    
    @classmethod
    def get(cls, key):
        if cls._config is not None:
            keys = key.split('.')  # Split the key into parts
            value = cls._config
            for k in keys:
                value = value.get(k)
                if value is None:
                    return None
            return value
        else:
            return None
        
    @classmethod
    def NewEnvConfig(cls):
        appName = cls.get('appName')
        serverPort = cls.get('serverPort')
        environment = cls.get('environment')
        releaseMode = cls.get('releaseMode')

        routeConfig = cls.get(f'route.{environment}')
        logConfig = cls.get(f'logDirectory.{environment}')
        databaseConfig = cls.get(f'database.{environment}')
        keyConfig = cls.get(f'key.{environment}')

        config = {
            'appName': appName,
            'serverPort': serverPort,
            'environment': environment,
            'releaseMode': releaseMode,

            'logDirectory': logConfig.get('path'),

            'connection': databaseConfig.get('connection'),
            'dialect': databaseConfig.get('dialect'),
            'username': databaseConfig.get('username'),
            'password': databaseConfig.get('password'),
            'url': databaseConfig.get('url'),
            'port': databaseConfig.get('port'),
            'schema': databaseConfig.get('schema'),
            
            'apiKey': keyConfig.get('apiKey'),
            'apiKeyEncode': keyConfig.get('apiKeyEncode'),
            'signatureKey': keyConfig.get('signatureKey'),
            'signatureKeyEncode': keyConfig.get('signatureKeyEncode'),

            'route': routeConfig.get('name'),
        }

        return config    