import time
import os
import base64 

from flask_bcrypt import Bcrypt
from flask import Flask, request, jsonify
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes

# Additional imports for cryptography
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

from src.constants import applicationConstant as constant

bcrypt = Bcrypt()

class CommonUtils:
    @staticmethod
    def GetCurrentDatetime():
        return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    
    @staticmethod
    def GeneralResponse(responseCode: int, responseDescription: str, responseDatas: list):
        responseData  = {
            'responseCode': responseCode,
            'responseDescription': responseDescription,
            'responseTime': CommonUtils.GetCurrentDatetime(),
            'responseDatas': responseDatas
        }

        response = jsonify(dict(responseData)) 
        response.status_code = responseCode 
        return response
    
    @staticmethod
    def GeneratePassword():
        password = "p@ssw0rd"
        return bcrypt.generate_password_hash(password).decode('utf-8')
    
    @staticmethod
    def HashPassword(password: str):
        hashPassword = bcrypt.generate_password_hash(password).decode('utf-8')
        return hashPassword

    @staticmethod
    def VerifyPassword(hashPassword: str, plainPassword: str):
        return bcrypt.check_password_hash(hashPassword, plainPassword)
    
    @staticmethod
    def validateJson(data, required_fields):
        for field in required_fields:
            if field not in data or not data[field]:
                return f"Field '{field}' is required and cannot be empty."

        return None
    
    @staticmethod
    def GenerateKey(password, salt):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            iterations=100000,
            salt=salt,
            length=32,
        )
        key = kdf.derive(password)
        return key
    
    @staticmethod
    def EncryptData(key, data):
        iv = os.urandom(16)
        cipher = Cipher(algorithms.AES(key), modes.GCM(iv), backend=default_backend())
        encryptor = cipher.encryptor()
        ciphertext = encryptor.update(data) + encryptor.finalize()
        return (iv + encryptor.tag + ciphertext)

    @staticmethod
    def DecryptData(key, data):
        iv = data[:16]
        tag = data[16:32]
        ciphertext = data[32:]
        cipher = Cipher(algorithms.AES(key), modes.GCM(iv, tag), backend=default_backend())
        decryptor = cipher.decryptor()
        plaintext = decryptor.update(ciphertext) + decryptor.finalize()
        return plaintext
    
    @staticmethod
    def DecryptAes256Sha256(encryptedData, keyType):
        if keyType == constant.DATA:
            password = constant.KEY_AES.encode('utf-8')
        else:
            password = constant.KEY_PASS_AES.encode('utf-8')

        # Decode the Base64-encoded data
        data = base64.b64decode(encryptedData.encode('utf-8'))

        # Extract the salt from the ciphertext
        salt = data[:16]
        ciphertext = data[16:]

        key = CommonUtils.GenerateKey(password, salt)
        decryptedData = CommonUtils.DecryptData(key, ciphertext)

        return decryptedData.decode('utf-8')

        
       